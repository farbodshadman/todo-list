# Simple Login and Sign Up

This project allows users to register and login using a simple login and sign-up functionality.

# Description

This project is built with Node.js and Express framework. User input is validated using the Joi library. For authentication, JWT (JSON Web Token) is used.

# Getting Started

## Dependencies

Make sure you have the following dependencies installed:

    Express
    MySQL2
    Dotenv
    Core
    JWT
    Joi
    Nodemon (optional)

To install the dependencies, run the following command:

npm install

## Frontend Repository
 This project is the backend component of a login and sign-up system. The frontend component can be found in the [mt-project-frontend](https://gitlab.com/Alhani/mt-project-frontend.git).

# Installation

To start the server, run the following command:

node server.js

or

npm run devstart

if you have Nodemon installed.

# Authors

    Alhan Hosseini
    Farbod Shadman

# License

This project is licensed under the [midterm-project-backend] License.

Feel free to customize the content based on your project's specific details. Make sure to replace [midterm-project-backend] with the appropriate license name or link.
