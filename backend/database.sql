CREATE DATABASE login;
USE login;

CREATE TABLE IF NOT EXISTS `users` (
  `id` INT AUTO_INCREMENT NOT NULL,
  `firstname` VARCHAR(50) NOT NULL,
  `lastname` VARCHAR(50) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS `todo_list` (
  `id` INT AUTO_INCREMENT NOT NULL,
  `user_id` INT,
  `title` VARCHAR(50) NOT NULL,
  `description` VARCHAR(150) NOT NULL,
  `due_date` DATETIME NOT NULL,
  `status` VARCHAR(50) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);