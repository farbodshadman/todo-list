require("dotenv").config();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const Users = require("../model/users");

const authenticationUser = async (req, res, next) => {
  try {
    const { email, password } = req.body;
    const result = await Users.loginUser(email);
    if (result.length === 0) {
      return res.status(401).json("*Invalid email or password.");
    } else {
      const passwordValidation = await bcrypt.compare(
        password,
        result[0].password
      );
      const user = {
        name: result[0].firstname + " " + result[0].lastname,
        email: email,
        id: result[0].id,
      };
      if (!passwordValidation) {
        return res.status(401).json("*Invalid email or password.");
      } else {
        const token = jwt.sign(user, process.env.TOKEN_SECRET_KEY, {
          expiresIn: "1h",
        });
        const decodeToken = jwt.decode(token);
        const expirationDate = new Date(decodeToken.exp * 1000);
        // console.log(new Date());
        // console.log(expirationDate.toISOString());
        res.json({
          accessToken: token,
          expirationDate: expirationDate.toISOString(),
          message: "Successful login!",
        });
      }
    }
  } catch (err) {
    next(err);
  }
};

module.exports = {
  authenticationUser,
};
