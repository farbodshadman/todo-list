const errorHandler = (error, req, res, next) => {
  if (!error.statusCode) {
    error.statusCode = 500;
    error.message = "Interval Server Error";
  }
  return res.status(error.statusCode).json(error.message);
};

module.exports = {
  errorHandler,
};
