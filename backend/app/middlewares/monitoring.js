const monitoring = async (req, res, next) => {
  try {
    date = new Date().toUTCString();
    console.log("Request path: " + req.path + " | " + date);
    console.log("Request method: " + req.method + " | " + date);
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = {
  monitoring,
};
