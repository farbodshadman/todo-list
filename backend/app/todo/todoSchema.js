const Joi = require("joi");
const { joiPasswordExtendCore } = require("joi-password");
const JoiPassword = Joi.extend(joiPasswordExtendCore);

const validator = (schema) =>
  async function (req, res, next) {
    try {
      await schema.validateAsync(req.body);
      next();
    } catch (err) {
      err.statusCode = 400;
      next(err);
    }
  };

const createTodoSchema = Joi.object({
  title: Joi.string().max(50).required(),
  description: Joi.string().max(200).required(),
  due_date: Joi.date(),
  status: Joi.string().max(50).required(),
});

const editTodoSchema = Joi.object({
  todo_id: Joi.number().required(),
  title: Joi.string().max(50),
  description: Joi.string().max(200),
  due_date: Joi.date(),
  status: Joi.string().max(50),
});

exports.validateCreateTodo = validator(createTodoSchema);
exports.validateEditTodo = validator(editTodoSchema);
