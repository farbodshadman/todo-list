const controllers = require("./controller");
const router = require("express").Router();
const { validateCreateTodo, validateEditTodo } = require("./todoSchema.js");
const {
  tokenAuthentication,
} = require("../middlewares/tokenAuthentication.js");

router.post(
  "/todo",
  validateCreateTodo,
  tokenAuthentication,
  controllers.createTodo
);

router.get("/todo/search", tokenAuthentication, controllers.getTodos);

router.put(
  "/todo/edit",
  validateEditTodo,
  tokenAuthentication,
  controllers.editTodo
);

router.delete("/todo/delete/:id", tokenAuthentication, controllers.deleteTodo);

module.exports = router;
