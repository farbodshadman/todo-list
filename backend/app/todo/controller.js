const Todo = require("../model/todo");

// create todo list
const createTodo = async (req, res, next) => {
  const user_id = req.user.id;
  const { title, description, due_date, status } = req.body;
  // due_date = new Date(due_date).toISOString().slice(0, 19).replace("T", " ");
  try {
    const newTodo = await Todo.createTodo(
      user_id,
      title,
      description,
      due_date,
      status
    );
    const [createdTodo] = await Todo.getTodoById(newTodo.insertId);
    res.status(200).json({
      messege: "Todo successfully created.",
      todo: createdTodo,
    });
  } catch (err) {
    next(err);
  }
};

// get todos
const getTodos = async (req, res, next) => {
  const user_id = req.user.id;
  try {
    let data = await Todo.getTodos(user_id);
    const searchTerm = req.query.t || "";
    if (searchTerm) {
      data = data.filter((data) => {
        const titleMatch = data.title
          .toLowerCase()
          .includes(searchTerm.toLowerCase());
        const descriptionMatch = data.description
          .toLowerCase()
          .includes(searchTerm.toLowerCase());
        return titleMatch || descriptionMatch;
      });
    }
    res.status(200).json({ result: data });
  } catch (err) {
    next(err);
  }
};

//get todo by id
const getTodoById = async (req, res, next) => {
  const todoId = req.body.todo_id;
  try {
    let data = await Todo.getTodoById(todoId);
    return data;
  } catch (err) {
    err.statusCode = 501;
    next(err);
  }
};

//edit todo
const editTodo = async (req, res, next) => {
  try {
    const [todo] = await getTodoById(req, res, next);
    if (req.user.id === todo.user_id) {
      if (req.body.title) {
        todo.title = req.body.title;
      }
      if (req.body.description) {
        todo.description = req.body.description;
      }
      if (req.body.due_date) {
        todo.due_date = req.body.due_date;
      }
      if (req.body.status) {
        todo.status = req.body.status;
      }
      const data = await Todo.editTodo(
        todo.id,
        todo.title,
        todo.description,
        todo.due_date,
        todo.status
      );
      if (data.affectedRows >= 0) {
        res.status(200).json({ todo });
      }
    } else {
      const err = new Error("Permission denied!");
      err.statusCode = 403;
      next(err);
    }
  } catch (err) {
    err.statusCode = 501;
    next(err);
  }
};

//delete todo
const deleteTodo = async (req, res, next) => {
  const todoId = req.params.id;
  try {
    const deletedTodo = await Todo.getTodoById(todoId);
    if (req.user.id === deletedTodo[0].user_id) {
      const data = await Todo.deleteTodo(todoId);
      if (deletedTodo.length < 1) {
        res.status(404).json({
          message: "Todo does not exist!",
        });
      }
      if (data.affectedRows > 0) {
        res.status(200).json({
          message: "Todo deleted successfully!",
          deletedTodo: deletedTodo,
        });
      }
    } else {
      const err = new Error("Permission denied!");
      err.statusCode = 403;
      next(err);
    }
  } catch (err) {
    next(err);
  }
};

module.exports = {
  getTodos,
  createTodo,
  editTodo,
  deleteTodo,
};
