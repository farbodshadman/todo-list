const express = require("express");
require("dotenv").config();
const app = express();
const UserRoutes = require("./users/routes");
const todoRoutes =require('./todo/routes')
const monitoring = require("../app/middlewares/monitoring");
const errorHandler = require("../app/middlewares/errorHandler");
const cors = require("cors");
app.use(cors());
app.use(express.json());

app.use(monitoring.monitoring);

app.get("/", (req, res) => {
  res.send("Server is running!");
});

app.use("/api/user", UserRoutes);
app.use('/api',todoRoutes)
app.use(errorHandler.errorHandler);

module.exports = app;
