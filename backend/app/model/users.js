const Database = require("../core/database");

const allUsers = async () => {
  const query = `SELECT * FROM login.users`;
  const [result] = await Database.query(query);
  const emails = [];
  for (i = 0; i < result.length; i++) {
    emails.push(result[i].email);
  }
  return emails;
};

// sign up
const createUser = async (firstname, lastname, email, password) => {
  const query = `
 INSERT INTO login.users (firstname,lastname,email,password) VALUES (?, ?, ?, ?);
`;
  const params = [firstname, lastname, email, password];
  const [result] = await Database.query(query, params);
  return result;
};

// get user
const getUser = async (userId) => {
  const query = `
  SELECT * FROM login.users WHERE id = ?;
  `;
  const [result] = await Database.query(query, [userId]);
  return result;
};

// login
const loginUser = async (email) => {
  const query = `SELECT * FROM login.users WHERE email = ?;`;
  const [result] = await Database.query(query, [email]);
  return result;
};

module.exports = {
  allUsers,
  createUser,
  getUser,
  loginUser,
};
