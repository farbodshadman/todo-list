const Database = require("../core/database");
// create to do list

const createTodo = async (user_id, title, description, due_date, status) => {
  const query = `
INSERT INTO login.todo_list (user_id, title, description, due_date, status) VALUES (?,?,?,?,?);
`;
  const params = [user_id, title, description, due_date, status];
  try {
    const [result] = await Database.query(query, params);
    return result;
  } catch (err) {
    next(err);
  }
};

// get list of todo
const getTodos = async (user_id) => {
  const query = `
    SELECT * FROM login.todo_list WHERE user_id = ?;
    `;
  const params = [user_id];
  try {
    const [result] = await Database.query(query, params);
    return result;
  } catch (err) {
    next(err);
  }
};

// get todo by id
const getTodoById = async (todoId) => {
  const query = `SELECT * FROM login.todo_list WHERE id = ?;`;
  const params = [todoId];
  try {
    const [result] = await Database.query(query, params);
    return result;
  } catch (err) {
    next(err);
  }
};

//edit todo
const editTodo = async (id, title, description, due_date, status) => {
  const query = `UPDATE login.todo_list SET title = ?, description = ?, due_date = ?, status = ? WHERE id = ?`;
  const params = [title, description, due_date, status, id];
  try {
    const [result] = await Database.query(query, params);
    return result;
  } catch (err) {
    next(err);
  }
};

//delete todo
const deleteTodo = async (todoId) => {
  const query = `DELETE FROM login.todo_list WHERE id = ?;`;
  const params = [todoId];
  try {
    const [result] = await Database.query(query, params);
    return result;
  } catch (err) {
    next(err);
  }
};

module.exports = {
  createTodo,
  getTodos,
  getTodoById,
  editTodo,
  deleteTodo,
};
