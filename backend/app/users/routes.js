const controllers = require("./controller.js");
const router = require("express").Router();
const { validateSignup, validateLogin } = require("./usersSchema.js");
const authenticate = require("../middlewares/authentication.js");
const tokenAuthenticate = require("../middlewares/tokenAuthentication.js");

router.post("/", validateSignup, controllers.createUser);
router.post("/login", validateLogin, authenticate.authenticationUser);
router.get(
  "/login",
  tokenAuthenticate.tokenAuthentication,
  controllers.loginUser
);
router.get("/:id", tokenAuthenticate.tokenAuthentication, controllers.getUser);

module.exports = router;
