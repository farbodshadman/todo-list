const Users = require("../model/users");
const bcrypt = require("bcrypt");

//sign up
const createUser = async (req, res, next) => {
  try {
    const { firstname, lastname, email, password } = req.body;
    const hashedPassword = await bcrypt.hash(password, 10);
    const emails = await Users.allUsers();
    if (emails.includes(email)) {
      const err = new Error("Email already exists!");
      err.statusCode = 400;
      next(err);
    } else {
      const insertionResult = await Users.createUser(
        firstname,
        lastname,
        email,
        hashedPassword
      );
      res.status(201).json({
        message: "User successfully created.",
        insertedId: insertionResult.insertId,
      });
    }
  } catch (err) {
    next(err);
  }
};

// get user
const getUser = async (req, res, next) => {
  try {
    const userId = req.params.id;
    const users = await Users.getUser(userId);
    if (!users) {
      const err = new Error("User not found!");
      err.statusCode = 404;
      next(err);
    } else {
      if (req.user.email !== users[0].email) {
        const err = new Error("You don't have access to this user's data!");
        err.statusCode = 403;
        next(err);
      } else {
        res.json(users[0]);
      }
    }
  } catch (err) {
    next(err);
  }
};

// log in
const loginUser = async (req, res, next) => {
  try {
    const email = req.user.email;
    const result = await Users.loginUser(email);
    if (!result) {
      const err = new Error("User not found!");
      err.statusCode = 404;
      next(err);
    } else {
      const print = {
        firstname: result[0].firstname,
        lastname: result[0].lastname,
        email: result[0].email,
      };
      res.json(print);
    }
  } catch (err) {
    next(err);
  }
};

module.exports = {
  createUser,
  getUser,
  loginUser,
};
