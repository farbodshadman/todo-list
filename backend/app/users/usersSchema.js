const Joi = require("joi");
const { joiPasswordExtendCore } = require("joi-password");
const JoiPassword = Joi.extend(joiPasswordExtendCore);

const validator = (schema) =>
  async function (req, res, next) {
    try {
      await schema.validateAsync(req.body);
      next();
    } catch (err) {
      err.statusCode = 400;
      next(err);
    }
  };

const signupSchema = Joi.object({
  firstname: Joi.string().max(50).required(),
  lastname: Joi.string().max(50).required(),
  email: Joi.string().max(50).email().required(),
  password: JoiPassword.string()
    .min(8)
    .max(50)
    .required()
    .minOfLowercase(2)
    .minOfUppercase(1)
    .minOfNumeric(2)
    .noWhiteSpaces()
    .messages({
      "string.min": "*Password must be at least 8 characters long.",
      "string.max": "*Password can not have more than 50 characters.",
      "password.minOfLowercase":
        "*Password should contain at least 2 lowercase characters.",
      "password.minOfUppercase":
        "*Password should contain at least 1 uppercase character.",
      "password.minOfNumeric":
        "*Password should contain at least 2 numeric characters.",
      "password.noWhiteSpaces": "*Password should not contain spaces.",
    }),
});

const loginSchema = Joi.object({
  email: Joi.string().max(50).email().required(),
  password: JoiPassword.string()
    .min(8)
    .max(50)
    .minOfLowercase(2)
    .minOfUppercase(1)
    .minOfNumeric(2)
    .noWhiteSpaces()
    .required()
    .messages({
      "string.min": "*Password must be at least 8 characters long.",
      "string.max": "*Password can not have more than 50 characters.",
      "password.minOfLowercase":
        "*Password should contain at least 2 lowercase characters.",
      "password.minOfUppercase":
        "*Password should contain at least 1 uppercase character.",
      "password.minOfNumeric":
        "*Password should contain at least 2 numeric characters.",
      "password.noWhiteSpaces": "*Password should not contain spaces.",
    }),
});

exports.validateSignup = validator(signupSchema);
exports.validateLogin = validator(loginSchema);
