import React from "react";
import { Link } from "react-router-dom";
import "./TokenExpire.css";

const ExpirationPage = () => {
  return (
    <div style={{ backgroundColor: "#1C2127" }}>
      <div className="message">You are not authorized.</div>
      <div className="message2">
        Your authentication has expired.
        <div>Please Log In again.</div>
        <div>
          <Link to="/">
            <button
              style={{
                backgroundColor: "#1F2B55",
                color: "white",
                borderRadius: "24px",
              }}
              className="button"
            >
              Log In
            </button>
          </Link>
        </div>
      </div>
      <div className="container">
        <div className="neon">403</div>
        <div className="door-frame">
          <div className="door">
            <div className="rectangle"></div>
            <div className="handle"></div>
            <div className="window">
              <div className="eye"></div>
              <div className="eye eye2"></div>
              <div className="leaf"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ExpirationPage;
