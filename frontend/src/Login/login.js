import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { useState } from "react";
import axios from "axios";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorEmail, setErrorEmail] = useState("");
  const [errorPassword, setErrorPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);

  const navigate = useNavigate();
  const body = {
    email,
    password,
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const data = await axios.post(
        `http://localhost:3000/api/user/login`,
        body
      );
      setErrorEmail("");
      setErrorPassword("");
      const token = [data.data.accessToken];
      if (token.length > 0) {
        localStorage.setItem("token", token[0]);
        localStorage.setItem("expire", data.data.expirationDate);
        const response = await axios.get(
          `http://localhost:3000/api/user/login`,
          {
            headers: {
              Authorization: `Bearer ${token[0]}`,
            },
          }
        );

        localStorage.setItem("firstname", response.data.firstname);
        localStorage.setItem("lastname", response.data.lastname);
        localStorage.setItem("email", response.data.email);
        navigate("/todo");
      }
    } catch (error) {
      const err = error.response.data;
      if (err === `"email" is not allowed to be empty`) {
        setErrorEmail("*Email is not allowed to be empty.");
      } else if (err === `"email" must be a valid email`) {
        setErrorEmail("*Email is not valid.");
      } else if (err === `Invalid email or password`) {
        setErrorPassword(`*${err}`);
      } else if (err === `"password" is not allowed to be empty`) {
        setErrorPassword("*Password is not allowed to be empty.");
      } else {
        setErrorPassword(err);
      }
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div
          className="hero is-fullheight"
          style={{ backgroundColor: "#1F2B55" }}
        >
          <div className="hero-body is-justify-content-center is-align-items-center">
            <div
              className="columns is-flex is-flex-direction-column box"
              style={{ backgroundColor: "#F5F5F5" }}
            >
              <h1 style={{ color: "#1F2B55", fontSize: "32px" }}>Log In</h1>
              <div className="column">
                <label htmlFor="email">Email</label>
                <input
                  style={{
                    borderColor: errorEmail ? "red" : "#1F2B55",
                    borderRadius: "8px",
                  }}
                  className="input"
                  type="email"
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                  name="email"
                  value={email}
                />
                <span style={{ color: "red" }}>{errorEmail}</span>
              </div>
              <div className="column" style={{ paddingBottom: "0" }}>
                <label htmlFor="Name">Password</label>
                <input
                  style={{
                    borderColor: errorPassword ? "red" : "#1F2B55",
                    borderRadius: "8px",
                  }}
                  className="input "
                  type={showPassword ? "text" : "password"}
                  onChange={(e) => {
                    setPassword(e.target.value);
                  }}
                  name="password"
                  value={password}
                />
                <span style={{ color: "red" }}>{errorPassword}</span>
              </div>
              <div style={{ textAlign: "right" }}>
                <button
                  type="button"
                  onClick={() => {
                    if (showPassword) {
                      setShowPassword(false);
                    } else {
                      setShowPassword(true);
                    }
                  }}
                  style={{
                    background: "none",
                    border: "none",
                    width: "94px",
                    fontSize: "12px",
                    marginRight: "10px",
                    marginBottom: "10px",
                  }}
                >
                  {showPassword ? "Hide Password" : "Show Password"}
                </button>
              </div>
              <div className="column">
                <button
                  style={{
                    backgroundColor: "#1F2B55",
                    color: "white",
                    borderRadius: "24px",
                  }}
                  className="button is-fullwidth"
                  type="submit"
                >
                  Log In
                </button>
              </div>
              <div className="has-text-centered">
                <p className="is-size-8">
                  {" "}
                  Don't have an account?{" "}
                  <Link
                    style={{ borderColor: "#1F2B55" }}
                    className="has-text"
                    to="/signup"
                  >
                    Sign Up
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};
export default Login;
