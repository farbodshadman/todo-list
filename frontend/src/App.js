import "bootstrap/dist/css/bootstrap.css";
import "bulma/css/bulma.css";
import React, { useEffect } from "react";
import { BrowserRouter, Routes, Route, useNavigate } from "react-router-dom";
import SignUp from "./SignUp/Signup";
import Login from "./Login/Login";
import Home from "./Home/HomePage";
import Todo from "./Todo/Todo";
import ExpirationPage from "./Login/Tokenexpire";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />}></Route>
        <Route path="/signup" element={<SignUp />}></Route>
        <Route path="/home2" element={<Home />}></Route>
        <Route path="/todo" element={<Todo />}></Route>
        <Route path="/token/expire" element={<ExpirationPage />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
