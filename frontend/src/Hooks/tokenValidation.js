import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

const useTokenValidation = () => {
  const navigate = useNavigate();
  useEffect(() => {
    const expire = localStorage.getItem("expire");
    const timeDiff = new Date(expire) - new Date();
    const timeOut = setTimeout(() => {
      navigate("/token/expire");
      console.log("Token Expired. Refreshing...");
    }, timeDiff);
    return () => {
      clearTimeout(timeOut);
    };
  }, []);
};

export default useTokenValidation;
