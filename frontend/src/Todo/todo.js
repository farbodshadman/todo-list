import "./Todo.css";
import CreateTask from "../Modal/CreateTask";
import { useEffect, useState } from "react";
import React from "react";
import Card from "./Card";
import { getTodo } from "../api/todo/get";
import useTokenValidation from "../Hooks/tokenValidation";

const Todo = () => {
  const [todoList, setTodoList] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [messageSerach, setMessageSerach] = useState("");
  useTokenValidation();

  const convertDate = (date) => {
    const year = new Date(date).getFullYear();
    const month = new Date(date).getMonth() + 1;
    const day = new Date(date).getDate();
    const convertedDate = year + "-" + "0" + month + "-" + day;
    return convertedDate;
  };

  const callGetTodo = () => {
    getTodo(searchTerm, (todos) => {
      if (todos.length === 0) {
        setMessageSerach("No match found.");
      } else {
        setMessageSerach("");
      }
      setTodoList(todos);
    });
  };

  useEffect(() => {
    callGetTodo();
  }, []);

  return (
    <div style={{ backgroundColor: "#e9eef6" }}>
      {
        <div>
          <div className="header text-center">
            <h3>Todo List</h3>
            <button
              className="btn btn-primery mt-2"
              onClick={() => {
                setShowModal(true);
              }}
            >
              Create Task
            </button>
            <div
              className="field has-addons mt-5"
              style={{ justifyContent: "center" }}
            >
              <div className="control">
                <input
                  className="input"
                  type="text"
                  placeholder="Find a task"
                  onChange={(e) => {
                    setSearchTerm(e.target.value);
                  }}
                  value={searchTerm}
                  onKeyDown={(event) => {
                    if (event.key === "Enter") {
                      callGetTodo();
                    }
                  }}
                />
              </div>
              <div className="control">
                <button
                  className="button is-info"
                  style={{ backgroundColor: "#1f2b55" }}
                  onClick={callGetTodo}
                >
                  Search
                </button>
              </div>
            </div>
          </div>
          <div className="task-container">
            {todoList.map((index) => {
              return (
                <Card
                  title={index.title}
                  description={index.description}
                  date={convertDate(index.due_date)}
                  status={index.status}
                  id={index.id}
                  key={index.id}
                  callGetTodo={callGetTodo}
                  convertDate={convertDate}
                />
              );
            })}

            <div
              className="text-center"
              style={{ color: "red", fontSize: "24px" }}
            >
              {messageSerach}
            </div>
          </div>{" "}
        </div>
      }

      {showModal && (
        <CreateTask
          setShowModal={setShowModal}
          callGetTodo={callGetTodo}
          convertDate={convertDate}
        />
      )}
    </div>
  );
};

export default Todo;
