import { useState } from "react";
import "./Card.css";
import { editTodo } from "../api/todo/put";
import { deleteTodo } from "../api/todo/delete";

const Card = ({
  title,
  description,
  date,
  status,
  id,
  callGetTodo,
  convertDate,
}) => {
  const color = [
    {
      primaryColor: "#007AC0",
      secondaryColor: "#DCE9F9",
    },
    {
      primaryColor: "#FA7D4C",
      secondaryColor: "#DCE9F9",
    },
    {
      primaryColor: "#A03E99",
      secondaryColor: "#DCE9F9",
    },
    {
      primaryColor: "#890707",
      secondaryColor: "#DCE9F9",
    },
    {
      primaryColor: "#348355",
      secondaryColor: "#DCE9F9",
    },
  ];
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [editTitle, setEditTitle] = useState(title);
  const [editDescription, setEditDescription] = useState(description);
  const [editDate, setEditDate] = useState(convertDate(date));
  const [editStatus, setEditStatus] = useState(status);
  const [titleError, setTitleError] = useState(false);
  const [descriptionError, setDescriptionError] = useState(false);
  const [dateError, setDateError] = useState(false);

  const overdue =
    Math.round(new Date(editDate).getTime() / 86400000) <
    Math.round(new Date().getTime() / 86400000);

  const handelDeleteClick = async () => {
    deleteTodo(
      id,
      (result) => {
        console.log(result);
        setShowDeleteModal(false);
        callGetTodo();
      },
      (error) => {
        console.log(error);
        setShowDeleteModal(false);
      }
    );
  };

  const handleEdit = async (e) => {
    e.preventDefault();
    if (editTitle === "") {
      setTitleError(true);
    }
    if (editDescription === "") {
      setDescriptionError(true);
    }
    if (editDate === "") {
      setDateError(true);
    }

    const body = {
      todo_id: id,
    };
    if (title !== editTitle) {
      body.title = editTitle;
    }
    if (description !== editDescription) {
      body.description = editDescription;
    }
    if (date !== editDate) {
      body.due_date = editDate;
    }
    if (status !== editStatus) {
      body.status = editStatus;
    }

    editTodo(
      body,
      (status) => {
        if (status === 200) {
          console.log("Todo edited successfully!");
        }
        setTitleError(false);
        setDescriptionError(false);
        setDateError(false);
        setShowEditModal(false);
        callGetTodo();
      },
      (error) => {
        if (error === '""title" is not allowed to be empty') {
          setEditTitle(error);
        }
      }
    );
  };

  const getColor = (status) => {
    if (status === "Done") {
      return "green";
    } else if (status === "In progress") {
      return "orange";
    } else {
      return "gray";
    }
  };

  const getIcon = (status) => {
    if (status === "Done") {
      return "fas fa-check";
    } else if (status === "In progress") {
      return "fas fa-spinner";
    } else {
      return "fas fa-tasks";
    }
  };

  const editModal = () => {
    return (
      <div className="modalBackground">
        <div className="modalContainter createModal">
          <form onSubmit={handleEdit}>
            <div className="form-group">
              <label>Task Name</label>
              <input
                type="text"
                className={titleError ? "form-control error" : "form-control"}
                value={editTitle}
                onChange={(e) => setEditTitle(e.target.value)}
              />
            </div>
            {titleError && (
              <p style={{ color: "red", width: "180px" }}>
                *This field is required!
              </p>
            )}
            <div className="form-group">
              <label>Description</label>
              <textarea
                rows="5"
                className={
                  descriptionError ? "form-control error" : "form-control"
                }
                value={editDescription}
                onChange={(e) => setEditDescription(e.target.value)}
              ></textarea>
            </div>
            {descriptionError && (
              <p style={{ color: "red", width: "180px" }}>
                *This field is required!
              </p>
            )}
            <div className="form-group">
              <label>Due Date</label>
              <input
                type="date"
                className={dateError ? "form-control error" : "form-control"}
                value={editDate}
                onChange={(e) => setEditDate(e.target.value)}
              />
            </div>
            {dateError && (
              <p style={{ color: "red", width: "180px" }}>
                *This field is required!
              </p>
            )}
            <div>
              <label>Status</label>
              <select
                className="form-control"
                value={editStatus}
                onChange={(e) => setEditStatus(e.target.value)}
                style={{
                  background: "transparent",
                  appearance: "none",
                  width: "180px",
                  paddingTop: "0px",
                  backgroundSize: "20px",
                  color: "#1f2b55",
                  fontSize: "20px",
                }}
              >
                <option style={{ background: "#ffffff" }}>Backlog</option>
                <option style={{ background: "#ffffff" }}>In progress</option>
                <option style={{ background: "#ffffff" }}>Done</option>
              </select>
            </div>
            <button
              className="mr-4 mt-4"
              type="submit"
              style={{
                width: "80px",
                height: "40px",
                backgroundColor: "#1f2b55",
                color: "white",
                borderRadius: "8px",
              }}
            >
              Edit
            </button>
            <button
              style={{
                width: "80px",
                height: "40px",
                backgroundColor: "red",
                border: "none",
                color: "white",
                borderRadius: "8px",
              }}
              onClick={() => {
                setEditTitle(title);
                setEditDescription(description);
                setEditDate(date);
                setEditStatus(status);
                setTitleError(false);
                setDescriptionError(false);
                setDateError(false);
                setShowEditModal(false);
              }}
            >
              Cancle
            </button>
          </form>
        </div>
      </div>
    );
  };

  return (
    <div>
      <div
        className={
          overdue
            ? "card-wrapper mr-5 mb-5 overdue-background"
            : "card-wrapper mr-5 mb-5"
        }
      >
        <div
          className="card-top"
          style={{ backgroundColor: color[id % 5].primaryColor }}
        ></div>
        <div className="task-holder">
          <span
            className="card-header"
            style={{
              backgroundColor: color[id % 5].secondaryColor,
              borderRadius: "10px",
              textAlign: "center",
              paddingLeft: "8px",
            }}
          >
            {title}
          </span>
          <div
            style={{
              color: getColor(status),
              fontSize: "16px",
              fontWeight: "bold",
              display: "flex",
              flexDirection: "row",
              gap: "4px",
              alignItems: "center",
            }}
          >
            <i className={getIcon(status)}></i>
            {status}
          </div>

          <p className="mt-4">{description}</p>
          <div className="mt-6">
            <h2
              style={{
                color: `${overdue ? "red" : color[id % 5].primaryColor}`,
                fontWeight: "bold",
              }}
            >
              Due-Date
            </h2>
            <p className={overdue ? "overdue-text" : ""}>{convertDate(date)}</p>
          </div>
          <div style={{ position: "absolute", right: "20px", bottom: "20px" }}>
            <i
              className="far fa-edit mr-3"
              style={{ color: color[id % 5].primaryColor, cursor: "pointer" }}
              onClick={() => setShowEditModal(true)}
            ></i>
            <i
              className="fas fa-trash-alt"
              style={{ color: color[id % 5].primaryColor, cursor: "pointer" }}
              onClick={() => setShowDeleteModal(true)}
            ></i>
          </div>
        </div>
      </div>
      {showDeleteModal && (
        <div className="deleteModal">
          <h2>Are you sure you want to delete the "{title}" task?</h2>
          <button
            className="mr-4 mt-4"
            style={{ backgroundColor: " #e0e0e0", color: "#333" }}
            onClick={() => setShowDeleteModal(false)}
          >
            No
          </button>
          <button
            className="mr-4 mt-4"
            style={{ backgroundColor: "#f44336", color: "white" }}
            onClick={handelDeleteClick}
          >
            Yes
          </button>
        </div>
      )}
      {showEditModal && editModal()}
    </div>
  );
};

export default Card;
