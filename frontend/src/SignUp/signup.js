import React from "react";
import { useState } from "react";
import axios from "axios";
import "./Signup.css";
import { Link, useNavigate } from "react-router-dom";

const SignUp = () => {
  const Navigate = useNavigate();
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordRepeat, setPasswordRepeat] = useState("");
  const [errorFirstname, setErrorFirstname] = useState("");
  const [errorLastname, setErrorLastname] = useState("");
  const [errorEmail, setErrorEmail] = useState("");
  const [errorPassword, setErrorPassword] = useState("");
  const passwordMatch = password === passwordRepeat;

  const handlePassword = () => {
    if (!passwordMatch) {
      return <h3 style={{ color: "red" }}>Passwords do not match</h3>;
    }
  };

  const body = {
    firstname,
    lastname,
    email,
    password,
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!passwordMatch) {
      alert("Entered passwords do not match, you can not sign up");
      return;
    } else {
      try {
        const data = await axios.post(`http://localhost:3000/api/user/`, body);
        // console.log(data);
        if (data.data.message === "user successfully created.") {
          Navigate("/");
        } else {
          console.log(data.data.message);
        }
      } catch (error) {
        console.log(error.response);
        console.log(error);
        const err = error.response.data;
        if (err === `"firstname" is not allowed to be empty`) {
          setErrorFirstname("*First name is not allowed to be empty.");
        } else if (err === `"lastname" is not allowed to be empty`) {
          setErrorLastname("*Last name is not allowed to be empty.");
        } else if (err === '"email" is not allowed to be empty') {
          setErrorEmail("*Email is not allowed to be empty.");
        } else if (err === '"email" must be a valid email') {
          setErrorEmail("*Email is not valid.");
        } else if (err === "Email already exists.") {
          setErrorEmail(`*${err}`);
        } else if (err === `"password" is not allowed to be empty`) {
          setErrorPassword("*Password is not allowed to be empty.");
        } else {
          setErrorPassword(err);
        }
      }
    }
  };

  return (
    <div>
      <form action="" onSubmit={handleSubmit}>
        <div
          className="hero is-fullheight"
          style={{ backgroundColor: "#1F2B55" }}
        >
          <div className="hero-body is-justify-content-center is-align-items-center">
            <div
              className="columns is-flex is-flex-direction-column box"
              style={{ backgroundColor: "#F5F5F5" }}
            >
              <h1 style={{ color: "#1F2B55", fontSize: "32px" }}>Sign Up</h1>
              <div className="column">
                <label htmlFor="name">First name</label>
                <input
                  style={{
                    borderColor: errorFirstname ? "red" : "#1F2B55",
                    borderRadius: "8px",
                  }}
                  className="input "
                  type="text"
                  name="name"
                  onChange={(e) => {
                    setFirstname(e.target.value);
                  }}
                  value={firstname}
                />
                <span style={{ color: "red" }}>{errorFirstname}</span>
              </div>
              <div className="column">
                <label htmlFor="name">Last name</label>
                <input
                  style={{
                    borderColor: errorLastname ? "red" : "#1F2B55",
                    borderRadius: "8px",
                  }}
                  className="input"
                  type="text"
                  name="name"
                  onChange={(e) => {
                    setLastname(e.target.value);
                  }}
                  value={lastname}
                />
                <span style={{ color: "red" }}>{errorLastname}</span>
              </div>
              <div className="column">
                <label htmlFor="email">Email</label>
                <input
                  style={{
                    borderColor: errorEmail ? "red" : "#1F2B55",
                    borderRadius: "8px",
                  }}
                  className="input"
                  type="email"
                  name="email"
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                  value={email}
                />
                <span style={{ color: "red" }}>{errorEmail}</span>
              </div>
              <div className="column">
                <label htmlFor="Name">Password</label>
                <input
                  className="input"
                  style={{
                    borderColor: errorPassword ? "red" : "#1F2B55",
                    borderRadius: "8px",
                  }}
                  type="password"
                  name="password"
                  onChange={(e) => {
                    setPassword(e.target.value);
                  }}
                  value={password}
                />
                <span style={{ color: "red" }}>{errorPassword}</span>
              </div>
              <div className="column">
                <label htmlFor="Name">Repeat Password</label>
                <input
                  style={{
                    borderColor: !passwordMatch ? "red" : "#1F2B55",
                    borderRadius: "8px",
                  }}
                  className="input"
                  type="password"
                  name="password"
                  onChange={(e) => {
                    setPasswordRepeat(e.target.value);
                  }}
                  value={passwordRepeat}
                />
                <span>{handlePassword()}</span>
              </div>
              <div className="column">
                <button
                  disabled={!passwordMatch}
                  style={{
                    backgroundColor: "#1F2B55",
                    color: "white",
                    borderRadius: "24px",
                  }}
                  className="button is-fullwidth"
                  type="submit"
                >
                  Create account
                </button>
              </div>
              <div className="has-text-centered">
                <p className="is-size-8">
                  {" "}
                  Already have an account?{" "}
                  <Link
                    to="/"
                    style={{ Color: "#1F2B55" }}
                    className="has-text"
                  >
                    Log In
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default SignUp;
