import axios from "axios";

export const getTodo = async (searchTerm, onSuccess) => {
  const token = localStorage.getItem("token");
  try {
    const response = await axios.get(
      `${process.env.REACT_APP_BASE_URL}/api/todo/search`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          t: searchTerm,
        },
      }
    );
    // console.log(response);

    const arrayList = response.data.result;
    onSuccess(arrayList);
  } catch (error) {
    console.log(error.message);
  }
};
