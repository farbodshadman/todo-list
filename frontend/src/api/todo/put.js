import axios from "axios";

export const editTodo = async (body, onSuccess, onError) => {
  try {
    const token = localStorage.getItem("token");
    const response = await axios.put(
      `${process.env.REACT_APP_BASE_URL}/api/todo/edit`,
      body,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    onSuccess(response.status);
  } catch (err) {
    onError(err.response.date);
  }
};
