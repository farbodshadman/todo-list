import axios from "axios";

export const deleteTodo = async (id, onSuccess, onError) => {
  try {
    const token = localStorage.getItem("token");
    const response = await axios.delete(
      `${process.env.REACT_APP_BASE_URL}/api/todo/delete/${id}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    onSuccess(response.data.message);
  } catch (error) {
    onError(error);
  }
};
