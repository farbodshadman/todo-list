import axios from "axios";

export const postTodo = async (
  title,
  description,
  due_date,
  status,
  onSuccess,
  onError
) => {
  try {
    const token = localStorage.getItem("token");
    const response = await axios.post(
      `${process.env.REACT_APP_BASE_URL}/api/todo`,
      {
        title,
        description,
        due_date,
        status,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    onSuccess(response);
  } catch (err) {
    onError(err.response.data);
  }
};
