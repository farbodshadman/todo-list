import React, { useState } from "react";
import "./CreateTask.css";
import { postTodo } from "../api/todo/post";

const CreateTask = ({ setShowModal, callGetTodo, convertDate }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [due_date, setDueDate] = useState("");
  const [status, setStatus] = useState("Backlog");
  const [errorTitlte, setErrorTitlte] = useState("");
  const [errorDescription, setErrorDescription] = useState("");
  const [errorDate, setErrorDate] = useState("");

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    postTodo(
      title,
      description,
      due_date,
      status,
      (result) => {
        console.log(result);
        setShowModal(false);
        callGetTodo();
      },
      (error) => {
        if (error === '"title" is not allowed to be empty') {
          setErrorTitlte(error);
        } else if (error === '"description" is not allowed to be empty') {
          setErrorTitlte("");
          setErrorDescription(error);
        } else if (error === '"due_date" must be a valid date') {
          setErrorTitlte("");
          setErrorDescription("");
          setErrorDate(error);
        }
      }
    );
  };

  return (
    <div>
      <div className="modalBackground">
        <div className="modalContainter createModal">
          <form onSubmit={handleFormSubmit}>
            <div className="form-group">
              <label>Task Name</label>
              <input
                style={{ borderColor: errorTitlte ? "red" : "" }}
                type="text"
                className="form-control"
                onChange={(e) => {
                  setTitle(e.target.value);
                }}
                value={title}
              />
            </div>
            <p style={{ color: "red", textAlign: "left" }}>{errorTitlte}</p>
            <div className="form-group">
              <label>Description</label>
              <textarea
                style={{ bordercolor: errorDescription ? " red" : "" }}
                rows="5"
                className="form-control"
                onChange={(e) => {
                  setDescription(e.target.value);
                }}
                value={description}
              ></textarea>
            </div>
            <p style={{ color: "red", textAlign: "left" }}>
              {errorDescription}
            </p>
            <div className="form-group">
              <label>Due Date</label>
              <input
                type="date"
                className="form-control"
                onChange={(e) => {
                  setDueDate(e.target.value);
                }}
                value={due_date}
              />
              <p style={{ color: "red", textAlign: "left" }}>{errorDate}</p>
            </div>
            <div>
              <label>Status</label>
              <select
                style={{
                  border: "none",
                  background: "transparent",
                  appearance: "none",
                  width: "180px",
                  paddingTop: "0px",
                  backgroundSize: "20px",
                  color: "#1f2b55",
                  fontSize: "20px",
                }}
                className="form-control"
                onChange={(e) => {
                  setStatus(e.target.value);
                }}
                value={status}
              >
                <option style={{ background: "#ffffff" }} value="Backlog">
                  Backlog
                </option>
                <option style={{ background: "#ffffff" }} value="In progress">
                  In progress
                </option>
                <option
                  style={{
                    background: "#ffffff",
                  }}
                  value="Done"
                >
                  Done
                </option>
              </select>
            </div>
            <button
              className="mr-4 mt-4"
              type="submit"
              style={{
                width: "80px",
                height: "40px",
                backgroundColor: "#1f2b55",
                color: "white",
                borderRadius: "8px",
              }}
            >
              Create
            </button>
            <button
              className="mt-4"
              style={{
                width: "80px",
                height: "40px",
                backgroundColor: "red",
                color: "white",
                borderRadius: "8px",
              }}
              onClick={() => setShowModal(false)}
            >
              Cancle
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateTask;
