import React from "react";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";

const Home = () => {
  const [isRefreshed, setIsRefreshed] = useState(false);
  const firstname = localStorage.getItem("firstname");
  const lastname = localStorage.getItem("lastname");
  const email = localStorage.getItem("email");

  const expire = localStorage.getItem("expire");

  useEffect(() => {
    if (new Date(expire) < new Date()) {
      isRefreshed(true);
      window.location.reload();
    }
  }, []);

  if (new Date(expire) > new Date()) {
    return (
      <div>
        <div
          className="hero is-fullheight"
          style={{ backgroundColor: "#1F2B55" }}
        >
          <div className="hero-body is-justify-content-center is-align-items-center">
            <div
              className="columns is-flex is-flex-direction-column box"
              style={{ backgroundColor: "#F5F5F5" }}
            >
              <h1 style={{ color: "#1F2B55", fontSize: "32px" }}>Welcome!</h1>
              <div className="column">
                <label htmlFor="email">Email:</label>

                <p>
                  <strong>{email}</strong>
                </p>
              </div>
              <div className="column">
                <label htmlFor="email">First name:</label>

                <p>
                  <strong>{firstname}</strong>
                </p>
              </div>
              <div className="column">
                <label htmlFor="email">Last name:</label>

                <p>
                  <strong>{lastname}</strong>
                </p>
              </div>

              <div className="has-text-centered">
                <p className="is-size-8">
                  Go back to{" "}
                  <Link
                    style={{ borderColor: "#1F2B55" }}
                    className="has-text"
                    to="/"
                  >
                    Log In
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div>
        <div
          className="hero is-fullheight"
          style={{ backgroundColor: "#1F2B55" }}
        >
          <div className="hero-body is-justify-content-center is-align-items-center">
            <div
              className="columns is-flex is-flex-direction-column box"
              style={{ backgroundColor: "#F5F5F5" }}
            >
              <h1 style={{ color: "#1F2B55", fontSize: "32px" }}>Welcome!</h1>
              <div className="column">
                <label htmlFor="email">Email:</label>
              </div>
              <div className="column">
                <label htmlFor="email">First name:</label>
              </div>
              <div className="column">
                <label htmlFor="email">Last name:</label>
              </div>
              <h1 className="text-danger">Your session has expired.</h1>
              <div className="has-text-centered">
                <p className="is-size-8">
                  Go back to{" "}
                  <Link
                    style={{ borderColor: "#1F2B55" }}
                    className="has-text"
                    to="/"
                  >
                    Log In
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};
export default Home;
