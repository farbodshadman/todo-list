# Authify

Authify is a project that enables users to register and login using a simple login and sign-up functionality.

# Description

This project is built with React.js, a popular JavaScript library for building user interfaces.

## Getting Started

### Dependencies

Make sure you have the following dependencies installed:

- axios
- bootstrap
- bulma
- react-router-dom

To install the dependencies, run the following command:


npm install

##  Backend Repository
 This project is the frontend component of a login and sign-up system. The backend component can be found in the [midterm-project-backend](https://gitlab.com/farbodshadman/midterm-project-backend.git).

# Installation

To start the application, run the following command:

npm start



# Authors

    Alhan Hosseini
    Farbod Shadman

# License

This project is licensed under the [mt-project-frontend] License.

Feel free to customize the content based on your project's specific details. Make sure to replace [mt-project-frontend] with the appropriate license name or link.
